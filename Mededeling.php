<?php

class Mededeling
{
    public $factuurnummer;
    public $klantennummer;
    public $mededeling;

    public function maakmededeling()
    {
        $factuur = str_pad($this->factuurnummer, 5, '0', STR_PAD_LEFT);
        $klant = str_pad($this->klantennummer, 5, '0', STR_PAD_LEFT);

        $tiendelig = $factuur . $klant;
//echo $tiendelig;
        $controle = $tiendelig % 97;
//echo $controle;
        $controle = str_pad($controle, 2, '0', STR_PAD_LEFT);
//echo $controle;
        if ($controle == 0) {
            $controle = 97;
        }
//echo $controle;
        $this->aangemaaktemededeling = $tiendelig . $controle;

//var_dump($mededeling);
        echo '<p class="message">De aangemaakte mededeling is: ' . $this->aangemaaktemededeling . '.</p>';
    }

    public function controleermededeling()
    {

        if (substr($this->mededeling, 0, 3) === "+++") {
            $eerteCijfer = substr($this->mededeling, 3, 3);
//    var_dump($eerteCijfer);
            $tweedeCijfer = substr($this->mededeling, 7, 4);
//    var_dump($tweedeCijfer);
            $derdeCijfer = substr($this->mededeling, 12, 5);
//    var_dump($derdeCijfer);
            $this->mededeling = $eerteCijfer . $tweedeCijfer . $derdeCijfer;
//    var_dump($mededeling);
        }

        $factuur = substr($this->mededeling, 0, 5);
//var_dump($factuur);
        $factuur = intval($factuur);
//var_dump($factuur);

        $klant = substr($this->mededeling, 5, 5);
//var_dump($klant);
        $klant = intval($klant);
//var_dump($klant);

        $controle = substr($this->mededeling, -2);
//var_dump($controle);
        $controle = intval($controle);
//var_dump($controle);

        $factuur = str_pad($factuur, 5, '0', STR_PAD_LEFT);
        $klant = str_pad($klant, 5, '0', STR_PAD_LEFT);

        $tiendelig = $factuur . $klant;
//echo $tiendelig;
        $rest = $tiendelig % 97;
//echo $rest;
        $rest = str_pad($rest, 2, '0', STR_PAD_LEFT);
//var_dump($rest);
        if ($rest == 0) {
            $rest = 97;
        }
//var_dump($controle);
//var_dump($rest);
if (isset($_POST['controleermededeling'])) {
        if ($controle == $rest) {
            echo "<p class='message'>" . $this->mededeling . " Dit is een geldig nummer</p>";
        } else {
            echo "<p class='message'>" . $this->mededeling . " Dit nummer is niet correct!!</p>";
        }
}
    }
}
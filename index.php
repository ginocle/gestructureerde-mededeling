<?php

require_once('initialize.php');


if(!empty($_POST['maakmededeling'])){
    $mededeling = new Mededeling;
    $mededeling->factuurnummer = $_POST['factuurnummer'];
    $mededeling->klantennummer = $_POST['klantennummer'];
    $mededeling->maakmededeling();
}

if(!empty($_POST['controleermededeling'])){
    $mededeling = new Mededeling;
    $mededeling->mededeling = $_POST['mededeling'];
    $mededeling->controleermededeling();
}

require 'views/index.view.php';
//dd($mededeling);
<!doctype html>
<html lang="nl">
<link rel="stylesheet" href="../css/style.css">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestructureerd nummer</title>
</head>
<body>

<h1>Controleer hier de geldigheid van uw gestructureerde mededeling:</h1>

<H2>Maak een mededeling</H2>
<form method="post">
    <input type="text" name="factuurnummer" id="factuurnummer" placeholder="Vul factuurnummer in">
    <input type="text" name="klantennummer" id="klantennummer" placeholder="Vul klantennummer in">
    <input type="submit" name="maakmededeling" value="Maak mededeling">
</form>

<H2>Controleer een mededeling</H2>
<form method="post">
    <input type="text" name="mededeling" id="mededeling" placeholder="Vul mededeling in">
    <input type="submit" name="controleermededeling" value="Controleer mededeling">
</form>

</body>
</html>